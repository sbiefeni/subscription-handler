const http = require('http')
const config = require('config')
const express = require('express')
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const logger = require('./util/log')
const { subscriptionRouter } = require('./routes/subscriptionRoutes')

// kafka services
const { initializePubSub } = require('./services/pubsubService')

// Constants
const PORT = 8080
const HOST = '0.0.0.0'
const VERSION = config.get('server.contextPath')
const CONTEXTROOT = '/'
const { NODE_ENV, NODE_CONFIG_ENV } = process.env

logger.debug(`Version=${VERSION}`)

// App
const app = express()

// Production best practices
app.use(helmet())

// Network Efficiency ++
app.use(compression())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(CONTEXTROOT, subscriptionRouter)

app.use((req, res) => {
  res.status(404).send("Sorry can't find that!")
})

app.use((err, req, res) => {
  logger.error(err.stack)
  res.status(500).send('Something broke!')
})

const server = http.createServer(app)

// initialize kafka consumer and producer
initializePubSub()

server.listen(PORT, HOST)
logger.info(`Running on http://${HOST}:${PORT}`)
logger.info(`NODE_ENV: ${NODE_ENV || 'NOT SET'}`)
logger.info(
  `NODE_CONFIG_ENV: ${NODE_CONFIG_ENV || 'NOT SET'}`,
)
