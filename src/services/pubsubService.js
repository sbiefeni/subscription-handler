const { Kafka, logLevel } = require('kafkajs')
const config = require('config')
const logger = require('../util/log')
const { subscriptionHandler } = require('../controllers/subscriptionController')

// constants
const { clientId, brokers, topics } = config.kafka

// create kafka
const kafka = new Kafka({ clientId, brokers, logLevel: logLevel.ERROR })
const producer = kafka.producer()
const consumer = kafka.consumer({ groupId: clientId })
const admin = kafka.admin()

/**
 * returns parsed JSON, or the original string
 *
 * @param {*} str
 * @return string || JSON
 */
function parseJson(str) {
  try {
    const parsed = JSON.parse(str)
    return parsed
  } catch (e) {
    return str
  }
}

/**
 * Route messages recieved from PubSub
 *
 * @param {*} message
 */
const messageHandler = async (message) => {
  logger.info('message handler got message')

  switch (message.messageType) {
    case topics.subscriptionProcess:
      subscriptionHandler(message)
      break
    default:
      logger.error('messageType not recognized, ignoring')
  }
}

/**
 * Initializes the consumer
 *
 */
const consume = async () => {
  await consumer.connect().catch((err) => {
    logger.error('error in consumer: ', err)
  })
  await consumer.subscribe({ topic: topics.subscriptionProcess })
  await consumer.run({
    // eslint-disable-next-line no-unused-vars
    eachMessage: async ({ topic, partition, message }) => {
      // each subscribed message will appear here
      try {
        const parsedMessage = parseJson(message.value.toString())
        logger.info(`received message: ${JSON.stringify(parsedMessage)}`)

        // forward message to message Handler
        messageHandler(parsedMessage)

      } catch (err) {
        logger.error(err.message)
      }
    },
  })
}

/**
 * publishes a message to specified topic
 * default topic is used if none specified
 *
 * @param {*} message
 * @param {*} [topic = topics.subscriptionProcess]
 */
const produce = async (message, topic = topics.subscriptionProcess) => {
  try {
    // send a message to the configured topic
    producer.send({
      topic,
      messages: [
        {
          value: message,
        },
      ],
    })
  } catch (err) {
    logger.error(`could not write message ${err.message}`)
  }
}

/**
 * Initialize connection to Kafka
 *
 * @param {*} [top=topics.subscriptionProcess]
 */
const initializePubSub = async (topic = topics.subscriptionProcess) => {
  await admin.connect() // TODO circular dependancy warning thrown here, unkown cause
  await admin.createTopics({
    waitForLeaders: true,
    topics: [
      { topic },
    ],
  })
  await producer.connect().catch((err) => {
    logger.error('error in producer: ', err)
  })
  await consume()

  producer.send({
    topic,
    messages: [
      {
        value: 'PubSub is online',
      },
    ],
  })
}

module.exports = {
  initializePubSub,
  produce,
}
