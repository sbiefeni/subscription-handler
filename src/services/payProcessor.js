const axios = require('../util/axiosConfig')

const url = 'https://testlink.c9pg.com:11911/restApi'
const testData = `
{
    "GMID": "1110222484",
    "GTID": "GT1120095178",
    "GMPW": "GMPW3010300378",
    "TransType": "Sale",
    "Medium": "Credit",
    "AccountNum": "4788250000028291",
    "ExpDate": "1222",
    "MainAmt": "1001",
    "TipAmt": "212",
    "IncTaxAmt": "153",
    "EntryMode": "Manual",
    "NeedSwipeCard": "N"
}
`

/**
 * Payment processing - I am completely simulating this process
 * as no part of the message is actually used.
 *
 * @param {*} message
 * @return {*}
 */
const processPayment = async (customerInfo) => {

  // load test data rather than customerInfo
  const data = testData
  // call a pay processor test API to simulate a transaction
  const result = await axios.post(
    url,
    data,
  ).catch((err) => {
    console.log(err)
    const sStatus = err.code || err.response.status
    const sMessage = err.details || err.message
    console.log(`${sStatus} - ${sMessage}`)
    return { status: 500 }
  })

  return result
}

module.exports = {
  processPayment,
}
