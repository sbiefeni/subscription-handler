const config = require('config')
const logger = require('../util/log')
const { incrementTries } = require('../util/utils')
// const { produce } = require('../services/pubsubService')
const { processPayment } = require('../services/payProcessor')

const { topics } = config.kafka

/**
 * Handle subscription messages
 *
 * @param {*} message
 */
const subscriptionHandler = async (message) => {
  logger.info('subscription handler got message')

  // this is a hack to avoid a circular dependancy warning - this is new for node and I need to evaluate
  const { produce } = require('../services/pubsubService')

  // try payment processor
  const result = await processPayment(message)

  if (result.status === 200 && result.data && result.data.AVSResultCode === '0') {
    // payment succeeded
    // send a message into pubsub that payment was successful
    message.messageType = topics.subscriptionSuccess
    logger.info(`Sending payment success message: ${JSON.stringify(message)}`)
    produce(JSON.stringify(message), topics.subscriptionSuccess)
  } else {
    // payment attempt failed, increment the retry count
    await incrementTries(message)
    logger.info(`Sending payment failure message: ${JSON.stringify(message)}`)
    produce(JSON.stringify(message), topics.subscriptionFail)
  }
}

module.exports = {
  subscriptionHandler,
}
