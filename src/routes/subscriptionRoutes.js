const subscriptionRouter = require('express').Router()
const { produce } = require('../services/pubsubService')

/**
 * take an API request and publishes to pubsub
 *
 * @param {*} req
 * @param {*} res
 */
subscriptionRouter.post('/', (req, res) => {
  const message = JSON.stringify(req.body)
  produce(message)
  res.status(200).send({ 'Processed Message: ': req.body })
})

module.exports = {
  subscriptionRouter,
}
