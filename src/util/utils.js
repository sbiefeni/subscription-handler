/* eslint-disable no-param-reassign */

const reMap = (obj, oldKey, newKey) => {
  const newObj = obj
  if (!obj[oldKey]) { return obj }
  newObj[newKey] = newObj[oldKey]
  delete newObj[oldKey]
  return newObj
}

/* special characters must be escaped */
function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace)
}

const incrementTries = async (message) => {
  message.payment.retry_count += 1
  return message
}

const createHeader = (token) => ({
  'Content-Type': 'application/json',
  Accept: '*/*',
  Authorization: `Bearer ${token}`,
})

const logResponse = (what, obj) => {
  if (!what) { return }
  obj = obj || ''
  const now = new Date()
  const data = JSON.stringify(obj)
  console.log(`\n${now.toISOString()} - ${what}: ${data}\n`)
}

const formatError = (code, reason, message) => ({
  code,
  reason,
  message,
})

module.exports = {
  reMap,
  replaceAll,
  createHeader,
  logResponse,
  formatError,
  incrementTries,
}
