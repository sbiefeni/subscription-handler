const https = require('https')

const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
  simple: false,
  resolveWithFullResponse: true,
  json: true,
})

const agent = httpsAgent

const axiosInstance = require('axios').create({
  timeout: 10000,
  proxy: false,
  headers: {
    'Content-Type': 'application/json',
  },
  httpsAgent: agent,
})

module.exports = axiosInstance
